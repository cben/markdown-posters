# Academic poster generation from Markdown

It can be hard to create academic presentations / posters with math equations using LibreOffice Impress or PowerPoint.
Plus, I'm a programmer and I firmly believe editing text can be a *better* experience than WYSIWYG.

At the other end of the spectrum, the LaTeX package [beamer](https://ctan.org/pkg/beamer) produces beautiful output with lots of control — but has a long learning curve, and is just so _verbose_.
Enter lightweight formats like Markdown.  To make a bullet list, I want to just write: `- Something.` not `\begin{itemize} \item Something. ...`.

I must warn all options below are [leaky abstractions](https://www.joelonsoftware.com/2002/11/11/the-law-of-leaky-abstractions/).

The easiest options for a poster I found (as of 2020):

## Markdown _inside_ LaTeX

One option that's pretty approachable, and supports real-time collaboration, is Overleaf with `\usepackage{markdown}`: https://www.overleaf.com/latex/examples/writing-posters-with-markdown/jtbgmmgqrqmh
With this approach, you'll use LaTeX/beamer commands like `\begin{column}` for high-level poster structure, but most of the inner content is markdown.

- You have total control over the TeX structure; for example you could also use other packages like [tikzposter instead of beamerposter](https://www.overleaf.com/learn/latex/Posters)...

Caveat: the `markdown` package implementation of the markdown syntax has some  weird aspects.  Its `hybrid` option is handy but doesn't really "understand" where you're using LaTeX constructs inside the markdown. For example the formula `$a_{i+1} + c_{j-1}$` does not render the `_` as subscripts, because it interpretted the `_..._` as markdown emphasis _before_ it parsed it as LaTeX math; you have to escape them `$a\_{i+1} + c\_{j-1}$`. 🤦

## Pandoc with `::: column`

Nowdays, there are tons of tools to convert Markdown to slides in both LaTeX/PDF and HTML, but [Pandoc](https://pandoc.org/) is the holy grail.

One of the (many) extensions to markdown syntax it supports is "[fenced divs](https://pandoc.org/MANUAL.html#divs-and-spans)", and recently it introduced a way to use is to group "frames" columns, which is exactly what you need for structuring a poster!

See [poster.markdown](https://gitlab.com/cben/markdown-posters/-/raw/master/poster.markdown) file for example of how the source looks — almost no traces of LaTeX!

To use this syntax, you need a very recent Pandoc as a [relevant bug](https://github.com/jgm/pandoc/issues/6033) has only been fixed in 2.9.2!

If you downloaded & installed fresh Pandoc, plus installed LaTeX, you can convert with:

```
pandoc poster.markdown --slide-level=2 --to=beamer --standalone --output=poster.pdf
```

but an easier way is to run a [pre-made container image](https://pandoc.org/installing.html#docker) that includes both pandoc and latex:

```
docker run --rm --volume="$PWD":/data --user=$(id -u):$(id -g) --security-opt=label=disable pandoc/latex:latest poster.markdown --slide-level=2 --to=beamer --standalone --output=poster.pdf
```

![poster.pdf as image](poster.png)

This directly does two steps (1) convert --to=beamer (2) run pdflatex to generate PDF.
In case you need to debug LaTeX issues, you can do only (1) to inspect the intermediate TeX source — just change the output extension: `--output=poster.tex`.

- For more control, see Pandoc manual.  You can insert raw LaTeX in the markdown.  You can modify the TeX template that Pandoc uses.  You can write a "pandoc filter" in various programming languages to further tweak the rendering...

### Pandoc can produce HTML too!  And tons of other formats...

The awesome consequence of writing a poster using only markdown syntax (with some Pandoc extensions) is that Pandoc can convert its intermediate representation to _tons_ of formats.
And it does support `::: columns` structure in at least some format.

It can produce plain HTML:

```
docker run --rm --volume="$PWD":/data --user=$(id -u):$(id -g) --security-opt=label=disable pandoc/latex:latest poster.markdown --slide-level=2 --standalone --katex --output=poster.html
```

and several flavours of slides HTML, e.g.

```
docker run --rm --volume="$PWD":/data --user=$(id -u):$(id -g) --security-opt=label=disable pandoc/latex:latest poster.markdown --slide-level=2 --to=slidy --standalone --katex --output=poster.slidy.html
```

Caveat: default HTML output links to some online libraries, e.g. for math it loads `katex` or `mathjax`.  Need some effort to get an HTML that will work offline (critical for conference slides, less for poster?).
