---
header-includes:
  # These go into the TeX "preamble".
  # paper size can go up to a0paper
  - \geometry{paper=a4paper,landscape}
# For available themes, see:
# https://mpetroff.net/files/beamer-theme-matrix/
# https://deic-web.uab.cat/~iblanes/beamer_gallery/
theme: Frankfurt
colortheme: crane
fonttheme: structurebold

# pdf-format is not pandoc's own option, but necessary for export with https://panwriter.com/ editor.
pdf-format: beamer
---

## Poster Title

H~2~O is a liquid.  2^10^ is 1024.

::: {.columns}

:::: {.column width=0.6}
::::: block
### Block 1 {.center}

| table | col  |
|-------|------|
| foo   | quux |
| $x^2$ | $$\sum_0^\infty y_n$$ |
:::::

Outside of block one, between.

::::: columns
:::::: column
### Block 2

- foo
- bar
::::::

:::::: column
### Block 3

Some code:
```python
def f():
    bar()
```
::::::
:::::

::::

:::: {.column width=0.3}
### Block 4

- foo

  Second paragraph of item.

- bar
  $$\begin{pmatrix}A & B \\ \begin{matrix}0 & 1 \\ 2 & 3\end{matrix} & D\end{pmatrix}$$

- quux
::::

:::

Outside allllllllllllllllllllllllllllllllllllllllllllllllll the blocks.
